README for pyADCCalib


The following package intends to pull raw data, decode, and calibrate the ADCs for protoDUNE and DUNE moving foward. These jobs are completed in modules that use pyROOT and gallery to pull the raw data and process it. 

The main console, pyADCCalib.py, is a wrapper that moderates a calibration from decoding to calibration plots.

Please ensure that the RunRawDecoder.fcl file has rce decoding enabled otherwise only photodector data will be collected. Furthermore, if rce data does not work then enable felix data and change the rce tag in pyCalibData.py.


Packages used:

dunetpc v06_70_00 and higher
gallery v1_07_02 and higher (includes pyROOT extension)


To operate:

Open pyADCCalib and place ROOT files for a specific run in an array to be decoded. Any special pulse height specifications need to be specified otherwise it is assumed that the DAC settings go from 2-7

Once the right files are inputted simply type the following:

python pyADCCalib.py

Ensure the relevant directories are setup in the plots folder if issues exist. The necessary subdirectories are labelled linreg, dnl_inl, and slopeHist.
