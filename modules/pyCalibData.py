# PyROOT script to pull decoded data for protoDUNE
# Richie Diurba, rdiurba@FNAL.GOV, 1 August 2018
import ROOT # Import all of ROOT to get gallery


def read_header(h): # Gallery extension for header files in PyROOT
        """Make the ROOT C++ jit compiler read the specified header."""
        ROOT.gROOT.ProcessLine('#include "%s"' % h)

def provide_get_valid_handle(klass): # Gallery extention to use pyROOT
        """Make the ROOT C++ jit compiler instantiate the
           Event::getValidHandle member template for template
           parameter klass."""
        ROOT.gROOT.ProcessLine('template gallery::ValidHandle<%(name)s> gallery::Event::getValidHandle<%(name)s>(art::InputTag const&) const;' % {'name' : klass})


read_header('gallery/ValidHandle.h') # Gallery header file

provide_get_valid_handle("std::vector<raw::RawDigit>"); # Gallery vector handle

rawdigits_tag=ROOT.art.InputTag("tpcrawdecoder:daq"); #Input tag for RCE data
#If RCE data DNE, but FELIX does, replace rcerawdecoder with felixrawdecoder

def getPeaks(data): # Get peaks for collection and induction channels
    i=0
    peakArray=[]
    startLoc=[]
    while i<(len(data)-500): # Pulse occurs ever 500 ticks
        peakLoc=i+data[i:(i+500)].index(max(data[i:(i+500)]))
        peak=max(data[i:(i+500)]) # Look for max within 500 peaks
	baseline=float(sum(data[(peakLoc-17):(peakLoc-7)])/10.) 
	# Measure baseline as an average of a series of ticks before peak
        if baseline<500:
	    baseline=data[peakLoc-17]
        peakArray.append(peak-baseline) # Calibration peak is peak-baseline
        i=i+500 
    std=[a**.5 for a in peakArray] # Std. assumed to be Poisson
    return peakArray, std # Return array for a channel in a dataset
def getTroughs(data): # Get troughs for induction channels
    i=0
    troughArray=[]
    while i<(len(data)-500): # See getPeaks
        peakLoc=i+data[i:(i+500)].index(max(data[i:(i+500)]))
        trough=min(data[peakLoc:(peakLoc+100)])
	baseline=float(sum(data[(peakLoc-17):(peakLoc-7)])/10.)
        if baseline<500:
	    baseline=data[peakLoc-17]
        troughArray.append(trough-baseline)
        i=i+500 
    std=[abs(a)**.5 for a in troughArray]
    return troughArray, std


def getDatasetPeaks(f, dac):
    # Get new handles for each dataset
    filename_vector = ROOT.vector(ROOT.string)(1, f)
    ev = ROOT.gallery.Event(filename_vector)
    print f
    # Capture the functions that will get ValidHandles. This avoids some
    # inefficiency in constructing the function objects many times.
    get_rawdigits = ev.getValidHandle(ROOT.vector(ROOT.raw.RawDigit))
    rawdigits=get_rawdigits(rawdigits_tag)
    
    nchannels=len(rawdigits.product()) # Number of channels in dataset
    print nchannels
    
    nticks=rawdigits.product()[0].Samples()-1 # Max number of ticks in event as set by RunRawDecoder.fcl
    j=0

    dacVal=[] # Arrays to append
    totPeaks=[]
    totPeaks_std=[]
    chn_no=[]
    while j<nchannels: # Loop through all channels and all ticks
        data=[]
        z=0
        while z<nticks:# Append channel data in an event
            data.append(int(rawdigits.product()[j].ADC(z)))
    	    #print data
	    z=z+1
        peakArray, std=getPeaks(data) # Find peaks and troughs in pulled data
        chn=int(rawdigits.product()[j].Channel())
        for pulsePeaks in peakArray:
            dacVal.append(dac)
            totPeaks.append(pulsePeaks)
            totPeaks_std.append(pulsePeaks**.5)
            chn_no.append(chn)
        if (chn<1600) or (chn>2560 and chn<2560+1600) or (chn>2560*2 and chn<2560*2+1600) or (chn>2560*3 and chn<2560*3+1600) or (chn>2560*4 and chn<2560*4+1600) or (chn>2560*5 and chn<2560*5+1600): # If chn no. in APA is less than 1700 then it is induction
            troughArray, std=getTroughs(data)
            for pulseTroughs in troughArray:
                dacVal.append(dac*-1)
        	totPeaks.append(pulseTroughs)
        	totPeaks_std.append(abs(pulseTroughs)**.5)
        	chn_no.append(chn)
        j=j+1
    return [chn_no, totPeaks, totPeaks_std, dacVal]

def getChargeColdBox(dac, gain, cap=183): # cap in fF
    dac=[i*gain*cap for i in dac] # mV*fF
    dac=[i/(1.6*10**2) for i in dac] # ke
    return dac

def getCharge(dac): # shaping in us
    if True:
        dac=[i*21.4 for i in dac] # ke
    return dac

def mean(data): # Functions for mean and std
    return sum(data)/len(data)



