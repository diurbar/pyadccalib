# PyROOT script to run RawDecoder from LArSoft for protoDUNE calibration
# Richie Diurba, rdiurba@FNAL.GOV, 1 August 2018
import os # Setup gallery and LArSoft, use os to initiate LArSoft jobs
def runDecoder(fileArray, rawDecoding=True): # Input is an array of raw files
    rawArray=[]
    for f in fileArray: 
        run_number=f[-23:-14] # Pull run number for ROOT
        output="/dune/data/users/rdiurba/RawData/output/RawDecoder_"+run_number+".root" # Create output label (edit for personal repo)
	if rawDecoding==True: # Variable to skip LArSoft if already decoded
            os.system("lar -c RunRawDecoder.fcl "+f+" -n 1 -o "+output) 
	rawArray.append(output) # Run job and append filename for bookkepping
    return rawArray    	
