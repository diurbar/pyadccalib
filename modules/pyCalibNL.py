


def dnl_inlCollection(x, y, slope, intercept):
    j=0  # Measure DNL and INL for collection
    dnl=[]
    inl=[]
    while j<(len(x)):
        d=y[j]-x[j]*slope-intercept
        dnl.append(abs(d))
        i=(x[j]+intercept/slope)/((1.)/slope)-y[j]
        
        inl.append(i)
        j=j+1
    x_set=list(set(x))
    i=0
    dnl_avg=[]
    inl_avg=[]
    for r in x_set:
	dnl_meas=[]
	inl_meas=[]
	i=0
        while i<len(x):
	    if r==x[i]:
	        dnl_meas.append(dnl[i])
		inl_meas.append(inl[i])
	    i=i+1
        dnl_avg.append(mean(dnl_meas))
	inl_avg.append(mean(inl_meas))
    dnl=dnl_avg
    inl=inl_avg
    return dnl, inl
def dnl_inlInduction(x, y, slope, intercept, slope2, intercept2):
    j=0 # Measure DNL and INL for induction channels
    dnl=[]
    inl=[]
    while j<(len(x)):
        if x[j]>0:
            d=y[j]-x[j]*slope-intercept#*x[j]/abs(x[j])
            dnl.append(abs(d))
            i=(x[j]+intercept/slope)/((1.)/slope)-y[j]
        
            inl.append(i)
	if x[j]<0:
	    d=y[j]-x[j]*slope2-intercept2#*x[j]/abs(x[j])
            dnl.append(abs(d))
            i=(x[j]+intercept2/slope2)/((1.)/slope2)-y[j]
            inl.append(-i)
        j=j+1
    x_set=list(set(x))
    i=0
    dnl_avg=[]
    inl_avg=[]
    for r in x_set:
	dnl_meas=[]
	inl_meas=[]
	i=0
        while i<len(x):
	    if r==x[i]:
	        dnl_meas.append(dnl[i])
		inl_meas.append(inl[i])
	    i=i+1
        dnl_avg.append(mean(dnl_meas))
	inl_avg.append(mean(inl_meas))
    dnl=dnl_avg
    inl=inl_avg
    return dnl, inl



def median(lst): # Function for median
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2

    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1])/2.0

def mean(data): # Functions for mean and std
    return sum(data)/len(data)
def std(data):
    avg=mean(data)
    ss=sum((i-avg)**2 for i in data)
    std=ss**.5/(len(data)-1)
    return std
