# PyROOT script to do linear fits and NL measurements for protoDUNE
# Richie Diurba, rdiurba@FNAL.GOV, 1 August 2018
from ROOT import gROOT, TFile,  TH1F, TMultiGraph, TTree, TCanvas, TGraphErrors, TF1, TGraph, TMath, TPaveStats
from array import array
gROOT.ProcessLine("gErrorIgnoreLevel=1001;")
def fitCollection(x, y, ey, channel, channelType, run_number='pdsptpc', printPlot=True): # Fit slopes for collection
    c1 = TCanvas( 'c1', "Lin Fit", 200, 10, 700, 500 ) # Canvas
    fit=TF1('linear',"pol0(0)+x*pol0(1)",x[0],x[-1]); # Linfit
    fit.SetParNames("Offset (counts)", "Slope (count/ke)"); # Label
    c1.SetFillColor( 0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(x)
    ex=[0]*len(x)
    ey=[4]*len(x)    # Artificial RMS to control chi-squared
    n = length;
    x  = array( 'f', x )
    ex = array( 'f', ex )
    y  = array( 'f', y )
    ey = array( 'f', ey)
    gr = TGraphErrors( n, x, y, ex, ey) # T1F functions to get fits, etc.
    ge=c1.GetListOfPrimitives().FindObject("Graph")
    title=("Linear Regression of DAC Pulses and ADC counts for channel "+str(channel)+" on APA5")
    gr.SetTitle(title)
    gr.SetMarkerColor( 4 )
    gr.GetXaxis().SetTitle("Electrons (ke)")
    gr.GetYaxis().SetTitle("ADC Output (Counts)")
    gr.SetMarkerStyle( 21 )
    pic_file="plots/linreg/"+str(run_number)+"_chn"+str(channel)+"_fitCalibration.png"
    gr.Fit('linear', 'Q')
    gr.Draw('AP')
    c1.Update()
    statBox = gr.GetListOfFunctions().FindObject("stats")
    statBox.SetX1NDC(.1)
    statBox.SetX2NDC(.5)
    slope=gr.GetFunction('linear').GetParameter(1) # Get parameters and return
    intercept=gr.GetFunction('linear').GetParameter(0);
    c1.Modified()
    if printPlot==True:
        c1.Print(pic_file)
    return slope, intercept
def fitInduction(x, y, ey, channel, channelType, run_number='pdsptpc', printPlot=True):
    i=0 # Due to poor DAC, do two fits for negative and positive bias
    dacPos=[] # Plot both fits on single Canvas
    adcPos=[]
    dacNeg=[]
    adcNeg=[]
    while i<len(y):
	if x[i]<0:
	    dacNeg.append(x[i])
	    adcNeg.append(y[i])
	if x[i]>0:
	    dacPos.append(x[i])
	    adcPos.append(y[i])
        i=i+1
    c1 = TCanvas( 'c1', "Lin Fit", 200, 10, 700, 500 )
    fit=TF1('linear',"pol0(0)+x*pol0(1)",0,max(dacPos));
    fit2=TF1('linear2',"pol0(0)+x*pol0(1)",min(dacNeg), 0);
    fit.SetParNames("Positive Offset (counts)", "Positive Slope (count/ke)");
    fit2.SetParNames("Negative Offset (counts)", "Negative Slope (count/ke)");
    c1.SetFillColor(0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(dacPos)
    ex=[0]*len(dacPos)
    ey=[4]*len(dacPos)    
    n = len(dacPos);
    x  = array( 'f', dacPos )
    ex = array( 'f', ex )
    y  = array( 'f', adcPos )
    ey = array( 'f', ey)
    n2=len(dacNeg)
    ex2=[0]*len(dacNeg)
    ey2=[4]*len(dacNeg)
    x2  = array( 'f', dacNeg )
    ex2 = array( 'f', ex )
    y2  = array( 'f', adcNeg )
    ey2 = array( 'f', ey)
    gr = TGraphErrors( n, x, y, ex, ey)
    gr2=TGraphErrors(n2, x2, y2, ex2, ey2)
    ge=c1.GetListOfPrimitives().FindObject("Graph")    
    title=("Linear Regression of DAC Pulses and ADC counts for channel "+str(channel)+" on APA5")
    gr.SetMarkerStyle( 21 )
    gr2.SetMarkerStyle( 21 )
    mg=TMultiGraph()
    mg.SetTitle(title)
    mg.Add(gr)
    mg.Add(gr2)
    gr.Fit('linear', 'Q')
    gr2.Fit('linear2', 'Q')
    mg.Draw('AP')
    mg.GetXaxis().SetTitle("Electrons (ke)")
    mg.GetYaxis().SetTitle("ADC Output (Counts)")
    mg.GetYaxis().SetTitleOffset(0.9) 
    channel=str(channel)
    c1.Update()
    statBox = gr.GetListOfFunctions().FindObject("stats")
    statBox2 = gr2.GetListOfFunctions().FindObject("stats")
    statBox.SetX1NDC(.1)
    statBox.SetX2NDC(.5)
    statBox2.SetY1NDC(.1)
    statBox2.SetY2NDC(.3)
    if len(channel)==1:
	channel='000'+str(channel)
    elif len(channel)==2:
	channel='00'+str(channel)
    elif len(channel)==3:
	channel='0'+str(channel)
    pic_file="plots/linreg/"+str(run_number)+"_chn"+str(channel)+"_fitCalibration.png"
    slope=gr.GetFunction('linear').GetParameter(1)
    intercept=gr.GetFunction('linear').GetParameter(0)
    #c1.SetBatch(kTRUE);
    c1.Modified()
    #c1.Update()
    if printPlot==True:
        c1.Print(pic_file)
    slope2=gr2.GetFunction('linear2').GetParameter(1)
    intercept2=gr2.GetFunction('linear2').GetParameter(0)
    return slope, intercept, slope2, intercept2
def histSlopes(slopes, channelType, run_number='pdsptpc'): # Create histogram of slopes
    slopes  = array( 'f', slopes )
    c1 = TCanvas( 'c1', "Slope Hist", 200, 10, 700, 500 )
    title="Histogram of gains for "+channelType+" channels"	
    h=TH1F("Histogram of Calibration Slopes",title,30,min(slopes),max(slopes))
    for i in slopes:
        h.Fill(i)
    
    h.GetXaxis().SetTitle("ADC Calibration Slope (count/ke)")
    filename="plots/slopeHist/"+run_number+'_'+channelType+"_gainHist.png"
    h.Draw()
    c1.Print(filename)
def histDNL(dnl, channelType, run_number='pdsptpc'): # Create histogram of differential non-linearity
    abs_dnl=[abs(j) for j in dnl]
    med_dnl=median(abs_dnl)
    med_dnl=round(med_dnl, 4)
    dnl  = array( 'f', dnl )
    c1 = TCanvas( 'c1', "DNL Hist", 200, 10, 700, 500 )
    string="Histogram of "+channelType+" |DNL| Measurements (Median:"+str(med_dnl)+')'
    h=TH1F("Abs(DNL)",string,100,0,20)
    for i in dnl:
        h.Fill(i)
    h.GetXaxis().SetTitle("DNL (Counts)")
    filename="plots/dnl_inl/"+run_number+"_"+channelType+"_DNL.png"
    h.Draw()
    c1.Print(filename)
def histINL(inl, channelType, run_number='pdsptpc'): # Create histogram of integral non-linearity
    abs_inl=[abs(j) for j in inl]
    med_inl=median(abs_inl)
    med_inl=round(med_inl, 4)
    inl  = array( 'f', inl )
    string="Histogram of "+channelType+" INL Measurements (Median |INL|:"+str(med_inl)+')'
    c1 = TCanvas( 'c1', "INL Hist", 200, 10, 700, 500 )
    h=TH1F("INL",string,100,-10,10)
    for i in inl:
        h.Fill(i)
    h.GetXaxis().SetTitle("INL (Counts)")
    filename="plots/dnl_inl/"+run_number+"_"+channelType+"_INL.png"
    h.Draw()
    c1.Print(filename)
def dnl_inlCollection(x, y, slope, intercept):
    j=0  # Measure DNL and INL for collection
    dnl=[]
    inl=[]
    while j<(len(x)):
        d=y[j]-x[j]*slope-intercept
        dnl.append(abs(d))
        i=(x[j]+intercept/slope)/((1.)/slope)-y[j]
        
        inl.append(i)
        j=j+1
    x_set=list(set(x))
    i=0
    dnl_avg=[]
    inl_avg=[]
    for r in x_set:
	dnl_meas=[]
	inl_meas=[]
	i=0
        while i<len(x):
	    if r==x[i]:
	        dnl_meas.append(dnl[i])
		inl_meas.append(inl[i])
	    i=i+1
        dnl_avg.append(mean(dnl_meas))
	inl_avg.append(mean(inl_meas))
    dnl=dnl_avg
    inl=inl_avg
    return dnl, inl
def dnl_inlInduction(x, y, slope, intercept, slope2, intercept2):
    j=0 # Measure DNL and INL for induction channels
    dnl=[]
    inl=[]
    while j<(len(x)):
        if x[j]>0:
            d=y[j]-x[j]*slope-intercept#*x[j]/abs(x[j])
            dnl.append(abs(d))
            i=(x[j]+intercept/slope)/((1.)/slope)-y[j]
        
            inl.append(i)
	if x[j]<0:
	    d=y[j]-x[j]*slope2-intercept2#*x[j]/abs(x[j])
            dnl.append(abs(d))
            i=(x[j]+intercept2/slope2)/((1.)/slope2)-y[j]
            inl.append(-i)
        j=j+1
    x_set=list(set(x))
    i=0
    dnl_avg=[]
    inl_avg=[]
    for r in x_set:
	dnl_meas=[]
	inl_meas=[]
	i=0
        while i<len(x):
	    if r==x[i]:
	        dnl_meas.append(dnl[i])
		inl_meas.append(inl[i])
	    i=i+1
        dnl_avg.append(mean(dnl_meas))
	inl_avg.append(mean(inl_meas))
    dnl=dnl_avg
    inl=inl_avg
    return dnl, inl
def slopePlot(x, y, channelType, run_number='pdsptpc'): # Slopes measured per channel
    c1 = TCanvas( 'c1', "Lin Fit", 200, 10, 700, 500 )
    c1.SetFillColor( 0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(x) 
    n = length;
    x  = array( 'f', x )
    y  = array( 'f', y )
    gr = TGraph( n, x, y)
    title=("Gain Measured Per Channel")
    gr.SetTitle(title)
    gr.SetMarkerColor( 4 )
    gr.GetYaxis().SetRangeUser(11, 14)
    gr.GetXaxis().SetTitle("ADC Channel")
    gr.GetYaxis().SetTitle("Gain (counts/ke)")
    
    gr.SetMarkerStyle( 21 )
    pic_file="plots/linreg/"+run_number+"_"+channelType+"_gainCalibration.png"
    gr.Draw( "a*" )
    #c1.SetBatch(kTRUE);
    c1.Modified()
    #c1.Update()
    c1.Print(pic_file)
def offsetPlot(x, y, channelType, run_number='pdsptpc'): # Histogram of linfit offsets
    c1 = TCanvas( 'c1', "Lin Fit", 200, 10, 700, 500 )
    c1.SetFillColor( 0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(x) 
    n = length;
    x  = array( 'f', x )
    y  = array( 'f', y )
    gr = TGraph( n, x, y)
    title=("Offset Measured Per Channel")
    gr.SetTitle(title)
    gr.SetMarkerColor( 4 )
    gr.GetYaxis().SetRangeUser(-400, 400)
    gr.GetXaxis().SetTitle("ADC Channel")
    gr.GetYaxis().SetTitle("Offset (counts)")
    
    gr.SetMarkerStyle( 21 )
    pic_file="plots/linreg/"+run_number+"_"+channelType+"_offsetCalibration.png"
    gr.Draw( "a*" )
    #c1.SetBatch(kTRUE);
    c1.Modified()
    #c1.Update()
    c1.Print(pic_file)
def peakRMSHist(dac, data, chn, channelType, run_number="pdsptpc"): # Plot RMS of peaks
    channel_set=list(set(chn))
    dac_set=list(set(dac))
    peak_std=[]
    peak_chn=[]
    peak_dac=[]
    for j in channel_set: # Loop through channels and get each peak
        for k in dac_set: # Loop through each dac setting
            i=0
	    peaks=[]
            while i<len(data):
                if chn[i]==j and dac[i]==k:
                    peaks.append(data[i])
	        i=i+1
	    peak_std.append(std(peaks))
	    peak_chn.append(j)
            #print peak_std
	    peak_dac.append(k)
    peak_std  = array( 'f', peak_std) # Make histogram
    string="Histogram of DAC Peak RMS Measurements for "+str(channelType)+" channels"
    c1 = TCanvas( 'c1', "RMS Hist", 200, 10, 700, 500 )
    h=TH1F("Histogram of RMS", string,60,0,5)
    for m in peak_std:
        h.Fill(m)
    
    h.GetXaxis().SetTitle("RMS (Counts)")
    filename="plots/peakRMS/"+run_number+"_"+channelType+"_rms.png"
    h.Draw()
    c1.Print(filename) 
		
def median(lst): # Function for median
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2

    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1])/2.0

def mean(data): # Functions for mean and std
    return sum(data)/len(data)
def std(data):
    avg=mean(data)
    ss=sum((i-avg)**2 for i in data)
    std=ss**.5/(len(data)-1)
    return std
