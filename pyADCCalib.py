# PyROOT wrapper to calibrate protoDUNE ADCs
# Richie Diurba, rdiurba@FNAL.GOV, 1 August 2018

import time, sys
from ROOT import gROOT, gStyle # ROOT configuration files
# Modules for the package
from modules import pyCalibPlot as plot # Module to plot
from modules import pyCalibData as data # Module to draw data from decoder
from modules import pyCalibDecoder as decoder # Module to run larsoft decoding
from modules import pyCalibDB as db # Module to create database


gStyle.SetOptStat() # Shows the mean, rms, and entries
gStyle.SetOptFit() # Another statbox configuration
gROOT.SetBatch(True) # Do not print graphs to screen

to=time.time() # Timer to monitor length to calibrate


#dac_setting=[2,3,4,6,7] # Pulser dac setting
#gain=14. # mV/fC calibration gain
#filenames=["/dune/data/users/rdiurba/RawData/raw/np04_raw_run003248_0001_dl1.root", "/dune/data/users/rdiurba/RawData/raw/np04_raw_run003249_0001_dl1.root", "/dune/data/users/rdiurba/RawData/raw/np04_raw_run003255_0002_dl1.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003256_0002_dl1.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003258_0001_dl1.root"] # Full pdsptpc last run 3258


dac_setting=[2,3,4,5, 6,7] # Pulser dac setting
gain=14. # mV/fC calibration gain
filenames=["/dune/data/users/rdiurba/RawData/raw/np04_raw_run003286_0001_dl1.root", "/dune/data/users/rdiurba/RawData/raw/np04_raw_run003285_0001_dl1.root", "/dune/data/users/rdiurba/RawData/raw/np04_raw_run003284_0001_dl1.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003289_0002_dl1.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003283_0001_dl1.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003282_0001_dl1.root"] # Full pdsptpc last run 3289


#dac_setting=[2,3,5, 6,7,8] # Pulser dac setting
#gain=14. # mV/fC calibration gain
#filenames=["/dune/data/users/rdiurba/RawData/raw/np04_raw_run003494_0001_dl1.root", "/dune/data/users/rdiurba/RawData/raw/np04_raw_run003497_0001_dl2.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003506_0002_dl1.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003501_0001_dl2.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003503_0001_dl2.root","/dune/data/users/rdiurba/RawData/raw/np04_raw_run003505_0003_dl1.root"] # Full pdsptpc last run 3506


if len(sys.argv)>1:
    dac_setting=sys.argv[1]

if len(sys.argv)>2:
    gain=sys.argv[2]


filenames=decoder.runDecoder(filenames) # Input files to be decoded
d=0 # DAC setting counter for loop
peak=[] # Empty arrays to append to make complete calibration dataset
channel=[]
dac=[]
peak_std=[]
run_number=filenames[-1][-14:-5]
for f in filenames: # For all decoded files, fill arrays with adc values
    results=data.getDatasetPeaks(f, dac_setting[d])
    i=0
    while i<len(results[0]):
        peak.append(results[1][i])
	channel.append(results[0][i])
        peak_std.append(results[2][i])
	dac.append(results[3][i])
	i=i+1
    d=d+1
dac_ke=data.getCharge(dac)
slopes_induct=[]
intercepts_induct=[]
dnl_induct=[]
inl_induct=[]
slopes_collect=[]
intercepts_collect=[]
dnl_collect=[]
inl_collect=[]
bad_channels=[]
channel_issues=[]
for i in list(set(channel)): # Measure linearities and NL for all channels
    j=0    
    x=[]
    y=[]
    ey=[]
    bad=0
    chn=i
    channelType='collection' # Label channel as induction or collection
    while j<len(peak):
        if i==channel[j]:
            x.append(dac_ke[j])
            y.append(peak[j])
            ey.append(peak_std[j])
    	 
        j=j+1
    if (chn<1600) or (chn>2560 and chn<2560+1600) or (chn>2560*2 and chn<2560*2+1600) or (chn>2560*3 and chn<2560*3+1600) or (chn>2560*4 and chn<2560*4+1600) or (chn>2560*5 and chn<2560*5+1600):
        channelType='induction'
    if channelType=='induction':
	chn_slope, chn_intercept, chn2_slope, chn2_intercept=plot.fitInduction(x, y, ey, i, channelType, run_number)
        slopes_induct.append(chn_slope)
        intercepts_induct.append(chn_intercept)
        slopes_induct.append(chn2_slope) # Append slope and intercepts
        intercepts_induct.append(chn2_intercept)
        chn_dnl, chn_inl=plot.dnl_inlInduction(x, y, chn_slope, chn_intercept, chn2_slope, chn2_intercept)
	tick=0
	x_set=list(set(x))
	while tick<len(y):
	    inl_index=x_set.index(x[tick])
	    if x[tick]<0:
		y[tick]=y[tick]-chn_inl[inl_index]-chn2_intercept
	    if x[tick]>0:
		y[tick]=y[tick]+chn_inl[inl_index]-chn_intercept
	    tick=tick+1
	run_number_calib=run_number+"_calib"
	chn_slope, chn_intercept, chn2_slope, chn2_intercept=plot.fitInduction(x, y, ey, i, channelType, run_number_calib)
	slopes_induct.append(chn_slope)
        intercepts_induct.append(chn_intercept)
        slopes_induct.append(chn2_slope) # Append slope and intercepts
        intercepts_induct.append(chn2_intercept)
        chn_dnl, chn_inl=plot.dnl_inlInduction(x, y, chn_slope, chn_intercept, chn2_slope, chn2_intercept)
        for m in chn_dnl:
	    dnl_induct.append(m)
	for m in chn_inl:
            inl_induct.append(m)
    else: # Same as above but only fit one slope due to collection
        chn_slope, chn_intercept=plot.fitCollection(x, y, ey, i, channelType, run_number)
        chn_dnl, chn_inl=plot.dnl_inlCollection(x, y, chn_slope, chn_intercept)
	tick=0
	x_set=list(set(x))
	while tick<len(y):
	    inl_index=x_set.index(x[tick])
	    y[tick]=y[tick]+chn_inl[inl_index]-chn_intercept
	    tick=tick+1
	run_number_calib=run_number+"_calib"
        chn_slope, chn_intercept=plot.fitCollection(x, y, ey, i, channelType, run_number_calib)
        chn_dnl, chn_inl=plot.dnl_inlCollection(x, y, chn_slope, chn_intercept)
	for m in chn_dnl:
            dnl_collect.append(m)
	for m in chn_inl:
            inl_collect.append(m)
	slopes_collect.append(chn_slope)
        intercepts_collect.append(chn_intercept)

plot.histSlopes(slopes_collect, 'collection', run_number)
plot.histINL(inl_collect, 'collection', run_number)
plot.histDNL(dnl_collect, 'collection', run_number)

plot.histSlopes(slopes_induct, 'induction', run_number)
plot.histINL(inl_induct, 'induction', run_number)
plot.histDNL(dnl_induct, 'induction', run_number)
print time.time()-to

def summaryROOTTree(data, run_number, plot=True):
    channel=data[0]
    slopes_collect=data[1]
    offset_collect=data[2]
    dnl_collect=data[3]
    inl_collect=data[4]
    slopes_induct=data[5]
    offset_induct=data[6]
    dnl_induct=data[7]
    inl_induct=data[8]
    # Plot slopes, DNL, and INL
    if plot==True:
        plot.histSlopes(slopes_collect, 'collection', run_number)
        plot.histINL(inl_collect, 'collection', run_number)
        plot.histDNL(dnl_collect, 'collection', run_number)

        plot.histSlopes(slopes_induct, 'induction', run_number)
        plot.histINL(inl_induct, 'induction', run_number)
        plot.histDNL(dnl_induct, 'induction', run_number)


